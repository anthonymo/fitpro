// plugins/vuetify.js
import '@mdi/font/css/materialdesignicons.css'
import { createVuetify, ThemeDefinition } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import * as labsComponents from 'vuetify/labs/components'
import { VDatePicker } from 'vuetify/labs/VDatePicker'

export default defineNuxtPlugin(nuxtApp => {
  const customDarkTheme: ThemeDefinition = {
    dark: true,
    colors: {
      // background: '#FFFFFF',
      // surface: '#FFFFFF',
      primary: '#0c0c0d',
      'primary-darken-1': '#3700B3',
      secondary: '#03DAC6',
      'secondary-darken-1': '#018786',
      error: '#B00020',
      info: '#2196F3',
      success: '#4CAF50',
      warning: '#FB8C00',
    },
  }

  const vuetify = createVuetify({
    ssr: true,
    components: {
      ...components,
      ...labsComponents,
      ...VDatePicker,
    },
    directives,
    theme: {
      themes: {
        customDarkTheme,
      },
      defaultTheme: 'customDarkTheme'
    },
    icons: {
      defaultSet: 'mdi', // This is already the default value - only for display purposes
    },
  })
  nuxtApp.vueApp.use(vuetify)
})