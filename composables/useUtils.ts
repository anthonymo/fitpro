//Manages logins and sign ups. 
//We'll use Magic Links, so users can sign in with their email without using passwords.

export const useUtils = () => {
    const sayHello = () => console.log("Hello from useUtils.")
    
    return {
      sayHello
    }
  }