export default defineNuxtRouteMiddleware((to, _from) => {
  const user = useSupabaseUser()
  if (!user.value) {
    setPageLayout('default')
  } else {
    setPageLayout('authenticated')
  }
})
