// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  modules: [
    '@pinia/nuxt', 
    '@nuxtjs/supabase'
  ],
  css: ['vuetify/lib/styles/main.sass'],
  build: {
    transpile: ['vuetify'],
  },
  supabase: {
    redirectOptions: {
      login: '/login',
      callback: '/home'
    },
    redirect: true
  },
  devtools: { enabled: true }
})
