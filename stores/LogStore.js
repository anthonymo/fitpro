import { defineStore } from "pinia"
import { useAuthStore} from './AuthStore'
import { useAppStore } from './AppStore'

export const useLogStore = defineStore({
  id: "best",
  state: () => ({
    authStore: useAuthStore(),
    appStore: useAppStore(),
    supabaseClient: useSupabaseClient(),
    personal: [],

  }),
  
  getters: {},
  actions: {
    async init() {
        this.getPersonally()
    },
    async getPersonally() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("personal")
          .select()
        if (error) throw error
        this.personal = data
        console.log(data)
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your Personal Best were succussfully fetched!')
      }
    },
    async updatePersonally(crossfit) {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from('personal')
          .update(crossfit)
          .eq("id", crossfit.id)
        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your Personal Best has been updated successfully.')
      }
    },
  },
})



//const { data, error } = await supabase
//  .from('docs')
//  .select('id, embedding, metadata')
//  .eq('url', '/hello-world')


//In this example, we are selecting the id, embedding, and metadata columns 
//from the docs table where the url column is equal to /hello-world. 
//The eq() function is used to specify the equality condition. 
//The data variable will contain the fetched data if the query is successful, 
//and the error variable will contain any errors that occurred during the query.

//Note that you can also fetch data from a foreign table, 
//which is a table in your database that maps to some data inside a remote server. 
//To create a foreign table, you can use the create foreign table SQL command. Here's an example of how to create a foreign table: