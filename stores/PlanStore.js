import { defineStore } from "pinia"
import { useAuthStore} from './AuthStore'
import { useAppStore } from './AppStore'

export const usePlanStore = defineStore({
  id: "plan",
  state: () => ({
    authStore: useAuthStore(),
    appStore: useAppStore(),
    supabaseClient: useSupabaseClient(),
    exerciseOne: '',
    exercise2: '',
    exercise3: '',
    exercise4: '',
    exercise5: '',
    exercise6: '',
    set1: 0,
    set2: 0,
    set3: 0,
    set4: 0,
    set5: 0,
    set6: 0,
    rep1: 0,
    rep2: 0,
    rep3: 0,
    rep4: 0,
    rep5: 0,
    rep6: 0,
  }),
  
  getters: {},
  actions: {
    async init() {
        this.getPlan()
    },
    async getPlan() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("plans")
          .select(
            "exercise_one, exercise_2, exercise_3, exercise_4, exercise_5, exercise_6, set_1, set_2, set_3, set_4, set_5, set_ 6, rep_1, rep_2, rep_3, rep_4, rep_5, rep_6"
          )
          .single()
        if (error) throw error
        this.exerciseOne = data.exercise_one
        this.exercise2 = data.exercise_2
        this.exercise3 = data.exercise_3
        this.exercise4 = data.exercise_4
        this.exercise5 = data.exercise_5
        this.exercise6 = data.exercise_6
        this.set1 = data.set_1
        this.set2 = data.set_2
        this.set3 = data.set_3
        this.set4 = data.set_4
        this.set5 = data.set_5
        this.set5 = data.set_6
        this.rep1 = data.rep_1
        this.rep2 = data.rep_2
        this.rep3 = data.rep_3
        this.rep4 = data.rep_4
        this.rep5 = data.rep_5
        this.rep6 = data.rep_6
        console.log(data)
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your PLAN was succussfully fetched!')
      }
    },
    async updatePlan() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("plans")
          .upsert({
            exercise_one: this.exerciseOne,
            exercise_2: this.exercise2,
            exercise_3: this.exercise3,
            exercise_4: this.exercise4,
            exercise_5: this.exercise5,
            exercise_6: this.exercise6,
            set_1: this.set1,
            set_2: this.set2,
            set_3: this.set3,
            set_4: this.set4,
            set_5: this.set5,
            set_6: this.set6,
            rep_1: this.rep1,
            rep_2: this.rep2,
            rep_3: this.rep3,
            rep_4: this.rep4,
            rep_5: this.rep5,
            rep_6: this.rep6
          })
        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your workout PLAN has been updated successfully.')
      }
    },
  },
})
