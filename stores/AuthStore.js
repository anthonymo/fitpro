import { defineStore } from "pinia"
import { useAppStore } from "./AppStore"

export const useAuthStore = defineStore({
  id: "auth",
  state: () => ({
    appStore: useAppStore(),
    supabaseClient: useSupabaseClient(),
    user: useSupabaseUser(),
    firstName: '',
    lastName: '',
    email: '',
    street: '',
    apt: '',
    city: '',
    country: '',
    postalCode: null,
    userName: '',
  }),
  getters: {},
  actions: {
    async signUp(email, password) {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient.auth.signUp({ 
          email: email,
          password: password,
        }) //the client data (email and password) are saved into the const. If error occurs then its caught. The reason for 
        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setSuccess('Registration was successfull. Please check your email to confirm your account.')
        this.appStore.setLoader(false)
      }
    },
    async signInWithPassword(email, password) {
      try {
        this.appStore.setLoader(true)
        const { error } = await this.supabaseClient.auth.signInWithPassword({
          email: email,
          password: password,
        })
        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        navigateTo("/profile")
        this.appStore.setLoader(false)
        this.appStore.setSuccess('You were successfully logged in.')
      }
    },
    async signInWithOtp() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await supabase.auth.signInWithOtp({
          email: email.value,
          options: {
            emailRedirectTo: "http://localhost:3000/home",
          },
        })
        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
        console.log("Please try again")
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('You were successfully logged in.')
      }
    },
    async handleSignOut() {
      try {
        this.appStore.setLoader(true)
        const { error } = await this.supabaseClient.auth.signOut()
        if (error) throw error
      } catch (error) {
        this.appStore.setLoader(false)
        this.appStore.setError(error)
      } finally {
        navigateTo("/login")
        this.appStore.setLoader(false)
        this.appStore.setSuccess('You were successfully logged out.')
      }
    },
    async updateProfile() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("profiles")
          .upsert({
            first_name: this.firstName,
            last_name: this.lastName,
            email: this.email,
            street: this.street,
            apt: this.apt,
            city: this.city,
            country: this.country,
            postal_code: this.postalCode,
            user_name: this.userName,
          })

        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your profile has been updated successfully. Please log in again.')
      }
    },
    async getUserProfile() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("profiles")
          .select(
            "first_name, last_name, street, email, apt, city, country, postal_code, user_name"
          )
          .single()
        if (error) throw error
        this.firstName = data.first_name
        this.lastName = data.last_name
        this.email = data.email
        this.street = data.street
        this.apt = data.apt
        this.city = data.city
        this.country = data.country
        this.postalCode = data.postal_code
        this.userName = data.user_name
        console.log(data)
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
      }
    },
  },
})
