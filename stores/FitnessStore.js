import { defineStore } from "pinia"
import { useAuthStore} from './AuthStore'
import { useAppStore } from './AppStore'

export const useFitnessStore = defineStore({
  id: "fitness",
  state: () => ({
    authStore: useAuthStore(),
    appStore: useAppStore(),
    supabaseClient: useSupabaseClient(),
    workouts: [],

  }),
  
  getters: {},
  actions: {
    async init() {
        this.getWorkouts()
    },
    async getWorkouts() {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("workouts")
          .select()
        if (error) throw error
        this.workouts = data
        console.log(data)
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your workouts were succussfully fetched!')
      }
    },
    async updateWorkout(hiit) {
      try {
        this.appStore.setLoader(true)
        const { data, error } = await this.supabaseClient
          .from("workouts")
          .update(hiit)
          .eq("id", hiit.id)
        if (error) throw error
      } catch (error) {
        this.appStore.setError(error)
      } finally {
        this.appStore.setLoader(false)
        this.appStore.setSuccess('Your workouts have been updated successfully.')
      }
    },
  },
})
