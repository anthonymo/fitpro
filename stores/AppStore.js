import { defineStore } from 'pinia'
import { useAuthStore } from './AuthStore'

export const useAppStore = defineStore({
  id: 'app',
  state: () => ({
    authStore: useAuthStore(),
    app: {
      navDrawer: true,
      navRail: false,
      darkMode: true,
      error: false,
      errorMessage: false,
      success: false,
      successMessage: false,
      loading: false
    }
  }),
  getters: {
  },
  actions: {
    async init () {
      this.getAppSettings()
    },
    async getAppSettings () {
      try {
        this.setLoader(true)
        const { data, error } = await this.authStore.supabaseClient
          .from('app')
          .select(`
            show_sidebar,
            show_rail,
            dark_mode
          `)
          .single()
        this.app.navDrawer = data.show_sidebar
        this.app.navRail = data.show_rail
        this.app.darkMode = data.dark_mode
        if (error) throw error
      } catch (error) {
        this.setError(error)
      } finally {
        this.setLoader(false)
      }
    },
    async switchDarkMode () {
      this.app.darkMode = !this.app.darkMode
      const { data, error } = await this.authStore.supabaseClient.from('app').upsert([{ dark_mode: this.app.darkMode }])
      if (error) throw error
    },
    async switchNavDrawer () {
      this.app.navDrawer = !this.app.navDrawer
      const { data, error } = await this.authStore.supabaseClient.from('app').upsert([{ show_sidebar: this.app.navDrawer }])
      if (error) throw error
    },
    async switchNavRail () {
      this.app.navRail = !this.app.navRail
      const { data, error } = await this.authStore.supabaseClient.from('app').upsert([{ show_rail: this.app.navRail }])
      if (error) throw error
    },
    setLoader (value) {
      this.app.loading = value
    },
    setError (value) {
      this.app.error = true
      this.app.errorMessage = value
    },
    unsetError () {
      this.app.error = false
      this.app.errorMessage = null
    },
    setSuccess (value) {
      this.app.success = true
      this.app.successMessage = value
    },
    unsetSuccess () {
      this.app.success = false
      this.app.successMessage = null
    }
  }
})